package package_1;

public class TimeHistory extends Sequence{

	protected double sensitivity;
	
	public TimeHistory(String device, String description, long date, int channelNr, String unit, double resolution,
			int length, double sensitivity) {
		super(device, description, date, channelNr, unit, resolution, length);
		this.sensitivity = sensitivity;
		// TODO Auto-generated constructor stub
	}
	
	public void mytoString(){
		System.out.println(this.device);
		System.out.println(this.description);
		System.out.println(this.date);
		System.out.println(this.channelNr);
		System.out.println(this.unit);
		System.out.println(this.resolution);
		System.out.println(this.length);
		System.out.println(this.sensitivity);
		System.out.println("\n");
	}

}
