package package_1;

public abstract class Sequence<T> extends Packet {

	protected int channelNr;
	protected String unit;
	protected double resolution;
	protected double length;
	protected T[] buffer;
	
	public Sequence(String device, String description, long date, int channelNr, String unit, double resolution, int length) {
		super(device, description, date);
		
		
		this.channelNr = channelNr;
		this.unit = unit;
		this.resolution = resolution;
		this.length = length;
		buffer = (T[])new Object[length];
		
		

	}

}
