package server_main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import interfaces_main.ICallback;
import interfaces_main.IData;

import java.nio.file.Files;

import package_1.Reserve;
import package_1.TimeHistory;
import package_1.Tools;

public class DataServant 
  extends UnicastRemoteObject 
  implements IData {
	private Map<Reserve, ICallback> records_sp = new HashMap<Reserve, ICallback>();
	private Map<TimeHistory, ICallback> records_th = new HashMap<TimeHistory, ICallback>();
	
  public DataServant() throws RemoteException {
  }  

  //register_sp - method registering Spectrum
  public boolean register_sp(Reserve name, ICallback n) throws RemoteException {
    System.out.println("Server.register_sp(): " + name);
    if (!records_sp.containsKey(name)) {
      records_sp.put(name, n); //put the spectrum object in the map
      String filename = name.mytoString2()+".spc"; //name of the file + file type
      try {
		Files.write(new File(filename).toPath(), Tools.serialize(name)); //write the file
	} catch (IOException e) {
		e.printStackTrace();
	} //save the file
      System.out.println("File saved");
      return true;
    }
    return false;
  }
  
  //register_th - method registering TimeHistory
  public boolean register_th(TimeHistory name, ICallback n) throws RemoteException {
	    System.out.println("Server.register_th(): " + name);
	    if (!records_th.containsKey(name)) {
	      records_th.put(name, n); //put the timehistory object in the map
	      String filename = name.mytoString2()+".thi"; //name of the file + file type
	      try {
			Files.write(new File(filename).toPath(), Tools.serialize(name));  //write the file
		} catch (IOException e) {
			e.printStackTrace();
		} //save the file
	      System.out.println("File saved");
	      return true;
	    }
	    return false;
	  }
  

	//method searching through spectra
	public Vector<String> search_sp(String name) throws RemoteException {
		Set<Reserve> set = records_sp.keySet(); //look in spectra records
		Vector<String> v = new Vector<String>();
		for(Reserve s : set)
			if(s.mytoString2().matches(".*"+name+".*")) //if name matches device_id, desc or date, add record to list of results
				v.add(s.mytoString2());
		return v;
	}
	
	//method searching through timehistories
	public Vector<String> search_th(String name) throws RemoteException {
		Set<TimeHistory> set = records_th.keySet(); //look in timehistory records
		Vector<String> v = new Vector<String>();
		for(TimeHistory s : set)
			if(s.mytoString2().matches(".*"+name+".*"))  //if name matches device_id, desc or date, add record to list of results
				v.add(s.mytoString2());
		return v;
	}
}