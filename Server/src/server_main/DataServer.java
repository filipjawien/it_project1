package server_main;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class DataServer {
	Registry reg;
	DataServant servant;
	
	public static void main(String[] args) {
		try {
		  new DataServer();
		} catch (Exception e) {
			e.printStackTrace(); 
			System.exit(1);
	} }
	
	protected DataServer() throws RemoteException {
		try { 
		  reg = LocateRegistry.createRegistry(1099); // Create name registry 
		  servant = new DataServant();        // Remote object		  
		  reg.rebind("DataServer", servant);  // bind name to the object
		  System.out.println("DataServer READY");
		} catch(RemoteException e) {
		  e.printStackTrace(); 
		  throw e;
	}	}
}

