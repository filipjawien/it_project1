package client_main;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Vector;

import interfaces_main.ICallback;
import interfaces_main.IData;
import package_1.Reserve;


import java.util.Scanner;



public class DataClient {
	
	private Scanner userInput = new Scanner(System.in);

	String a = userInput.next();
	String b = userInput.next();
	long c = userInput.nextInt();
	int d = userInput.nextInt();
	String e = userInput.next();
	Reserve Reserve_obj = new Reserve(a, b, c, d, e, 1, 1, "h"); 

	IData remoteObject;
	ICallback callback;
	public static void main(String[] args) {
		System.out.println("Enter movie name, day of showing, date of showing, number of reservations and numbers of seats");
		System.out.println(" ");
	  	if(args.length < 1) {
			System.out.println("Usage: DataClient <server host name>");
			System.exit(-1);
		}
		new DataClient(args[0]);
    }

	public DataClient(String hostname) {
		Registry reg;    // object names registry
		try {
			reg = LocateRegistry.getRegistry(hostname); //get reference to names of objects
			//System.out.println(hostname);
			remoteObject = (IData) reg.lookup("DataServer"); //connect with remote object
			loop2(); //execute the main UI function
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
	}

	void search_sp() {
		String line;
		System.out.print("Enter movie, day or date:");
		if(userInput.hasNextLine()) {
			line = userInput.nextLine();
			Vector<String> vec = null;
			try {
				vec = remoteObject.search_sp(line); //connect with function in remote object
			} catch (RemoteException e) { e.printStackTrace(); }
			System.out.println("There are " + vec.size() + " record(s):"); //display results
			for (String s : vec)
				System.out.println(" - " + s);
		}
	}
	

	
	void loop2() {
		while(true) {
			String line;
			System.out.println("[c]reate reservation or [s]earch through my reservations"); //what user wants to do
			if(userInput.hasNextLine()) {
				line = userInput.nextLine();
				if(!line.matches("[cs]")) {
					System.out.println("You entered invalid command!"); //check if proper command
					continue;
				}
				switch (line) {
					case "c":
						String line3 = null;
						System.out.println("[r]eserve"); //what kind of record
						if(userInput.hasNextLine()) {
							line3 = userInput.nextLine();
							if(!line3.matches("[r]")) {
								System.out.println("You entered invalid command!");
								continue;
							}
							switch (line3) {
								case "r":
								
								try {
									remoteObject.register_sp(Reserve_obj, callback); //register Reserve object
							
								} catch (RemoteException e) {
									e.printStackTrace();
								}
									System.out.println(Reserve_obj.mytoString2()); //display info
									break;

						}}
						
						break;

					case "s":
						String line2 = null;
						System.out.println("[r]servations?"); //what records to search?
						if(userInput.hasNextLine()) {
							line2 = userInput.nextLine();
							if(!line2.matches("[r]")) {
								System.out.println("You entered invalid command!");
								continue;
							}
							switch (line2) {
								case "r":
									search_sp(); //search through reservations
									break;

						}
						
						break;
				}
			}
		}

	}
	
	
}}

