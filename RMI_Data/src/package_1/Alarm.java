package package_1;

public class Alarm extends Packet{

	protected int channelNr;
	protected int threshold;
	protected int direction;
	public Alarm(String device, String description, long date, int channelNr, int threshold, int direction) {
		super(device, description, date);
		// TODO Auto-generated constructor stub
		this.channelNr = channelNr;
		this.threshold = threshold;
		this.direction = direction;
	}
	
	public void mytoString(){
		//return this.channelNr + this.threshold + this.direction;
		System.out.println(this.device);
		System.out.println(this.description);
		System.out.println(this.date);
		System.out.println(this.channelNr);
		System.out.println(this.threshold);
		System.out.println(this.direction);
		System.out.println("\n");
	}

}
