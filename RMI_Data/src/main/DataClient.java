package main;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Vector;

import package_1.Reserve;
import package_1.TimeHistory;

import java.util.Scanner;

public class DataClient {
	private Scanner userInput = new Scanner(System.in);
	Reserve spectrum_obj = new Reserve("device_1", "spectrum_1", 10102015, 5, "Hz", 534.3, 100, "logarithmic");
	TimeHistory thist_obj = new TimeHistory("device_1", "timehistory_1", 15052012,5 ,"Hz", 220.3,100, 23412.2);
	 //exemplary objects, so there is no need to type data each time
	IData remoteObject;
	ICallback callback;
	public static void main(String[] args) {
	  	if(args.length < 1) {
			System.out.println("Usage: DataClient <server host name>");
			System.exit(-1);
		}
		new DataClient(args[0]);
    }

	public DataClient(String hostname) {
		Registry reg;    // object names registry
		try {
			reg = LocateRegistry.getRegistry(hostname); //get reference to names of objects
			//System.out.println(hostname);
			remoteObject = (IData) reg.lookup("DataServer"); //connect with remote object
			callback = new ClientCallback();
			loop2(); //execute the main UI function
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
	}

	void search_sp() {
		String line;
		System.out.print("Enter device, description or date:");
		if(userInput.hasNextLine()) {
			line = userInput.nextLine();
			Vector<String> vec = null;
			try {
				vec = remoteObject.search_sp(line); //connect with function in remote object
			} catch (RemoteException e) { e.printStackTrace(); }
			System.out.println("There are " + vec.size() + " record(s):"); //display results
			for (String s : vec)
				System.out.println(" - " + s);
		}
	}
	
	void search_th() {
		String line;
		System.out.print("Enter device, description or date:");
		if(userInput.hasNextLine()) {
			line = userInput.nextLine();
			Vector<String> vec = null;
			try {
				vec = remoteObject.search_th(line);  //connect with function in remote object
			} catch (RemoteException e) { e.printStackTrace(); }
			System.out.println("There are " + vec.size() + " record(s):");  //display results
			for (String s : vec)
				System.out.println(" - " + s);
		}
	}
	
	void loop2() {
		while(true) {
			String line;
			System.out.println("[c]reate record or [s]earch through them?"); //what user wants to do
			if(userInput.hasNextLine()) {
				line = userInput.nextLine();
				if(!line.matches("[cs]")) {
					System.out.println("You entered invalid command!"); //check if proper command
					continue;
				}
				switch (line) {
					case "c":
						String line3 = null;
						System.out.println("[s]pectrum or [t]imehistory?"); //what kind of record
						if(userInput.hasNextLine()) {
							line3 = userInput.nextLine();
							if(!line3.matches("[st]")) {
								System.out.println("You entered invalid command!");
								continue;
							}
							switch (line3) {
								case "s":
								
								try {
									remoteObject.register_sp(spectrum_obj, callback); //register spectrum object
								} catch (RemoteException e) {
									e.printStackTrace();
								}
									System.out.println(spectrum_obj.mytoString2()); //display info
									break;
								case "t":

									try {
										remoteObject.register_th(thist_obj, callback); //register timehistory object
									} catch (RemoteException e) {
										e.printStackTrace();
									}
										System.out.println(thist_obj.mytoString2()); //display info
									break;
								
							}
						}
						
						break;

					case "s":
						String line2 = null;
						System.out.println("[s]pectrum or [t]imehistory?"); //what records to search?
						if(userInput.hasNextLine()) {
							line2 = userInput.nextLine();
							if(!line2.matches("[st]")) {
								System.out.println("You entered invalid command!");
								continue;
							}
							switch (line2) {
								case "s":
									search_sp(); //search through spectra
									break;
								case "t":
									search_th(); //search through timehistories
									break;
								
							}
						}
						
						break;
				}
			}
		}

	}
	
	
}

