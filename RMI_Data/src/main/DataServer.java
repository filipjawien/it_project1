package main;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class DataServer {
	Registry reg; // rejestr nazw obiektow
	DataServant servant; // klasa uslugowa
	
	public static void main(String[] args) {
		try {
		  new DataServer();
		} catch (Exception e) {
			e.printStackTrace(); 
			System.exit(1);
	} }
	
	protected DataServer() throws RemoteException {
		try { 
		  reg = LocateRegistry.createRegistry(1099); // Utworzenie rejestru nazw  
		  servant = new DataServant();        // utworzenie zdalnego obiektu		  
		  reg.rebind("DataServer", servant);  // zwiazanie nazwy z obiektem
		  System.out.println("DataServer READY");
		} catch(RemoteException e) {
		  e.printStackTrace(); 
		  throw e;
	}	}
}

