package interfaces_main;

import java.rmi.*;
import java.util.Vector;

import package_1.Reserve;
import package_1.TimeHistory;

public interface IData extends Remote {
   boolean register_sp(Reserve name, ICallback n) throws RemoteException;
   boolean register_th(TimeHistory name, ICallback n) throws RemoteException;
   Vector<String> search_sp(String name) throws RemoteException;
   Vector<String> search_th(String name) throws RemoteException;
}
